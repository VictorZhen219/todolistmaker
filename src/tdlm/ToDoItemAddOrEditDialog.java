package tdlm;

import java.net.URL;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static tdlm.PropertyType.*;
import tdlm.data.ToDoItem;

/**
 *
 * @author Victor Zhen
 */
public class ToDoItemAddOrEditDialog extends Stage{
    
    final int WIDTH = 350;
    final int HEIGHT = 225;
    final String ADD_EDIT_DIALOG = "add_edit_dialog";
    PropertiesManager manager;
    
    GridPane gPane;
    TextField categoryField;
    TextField descriptionField;
    DatePicker startDate;
    DatePicker endDate;
    CheckBox cBox;
    Button yesButton;
    Button noButton;
    
    boolean yesOrNo = true;
    
    public ToDoItemAddOrEditDialog(){
    //organize the initialization here
        manager = PropertiesManager.getPropertiesManager();
    
        gPane = new GridPane();
        categoryField = new TextField();
        descriptionField = new TextField();
        startDate = new DatePicker();
        endDate = new DatePicker();
        startDate.setValue(LocalDate.now());
        endDate.setValue(LocalDate.now());
        cBox = new CheckBox();
        yesButton = new Button(manager.getProperty(PropertyType.OK));
        noButton = new Button(manager.getProperty(PropertyType.CANCEL));
        
        initStyle();
        initListeners();
    }
    
    //overloaded constructor
    //@param item - the item that has the values to place back into the
    //fields
    public ToDoItemAddOrEditDialog(ToDoItem item){
    //organize the initialization here
        manager = PropertiesManager.getPropertiesManager();
    
        gPane = new GridPane();
        categoryField = new TextField(item.getCategory());
        descriptionField = new TextField(item.getDescription());
        startDate = new DatePicker(item.getStartDate());
        endDate = new DatePicker(item.getEndDate());
        cBox = new CheckBox();
        cBox.setSelected(item.getCompleted());
        yesButton = new Button(manager.getProperty(PropertyType.OK));
        noButton = new Button(manager.getProperty(PropertyType.CANCEL));
        
        initStyle();
        initListeners();
    }
        
    private void initStyle(){
        //initialize the gridpane to contain everything.
        gPane.setPrefSize(WIDTH, HEIGHT);
        gPane.add(new Label(manager.getProperty(PropertyType.CATEGORY_COLUMN_HEADING)), 0, 0);
        gPane.add(new Label(manager.getProperty(PropertyType.DESCRIPTION_COLUMN_HEADING)), 0, 1);
        gPane.add(new Label(manager.getProperty(PropertyType.START_DATE_COLUMN_HEADING)), 0, 2);
        gPane.add(new Label(manager.getProperty(PropertyType.END_DATE_COLUMN_HEADING)), 0, 3);
        gPane.add(new Label(manager.getProperty(PropertyType.COMPLETED_COLUMN_HEADING)), 0, 4);
        
        gPane.add(yesButton, 0, 5);
        gPane.add(categoryField, 1, 0);
        gPane.add(descriptionField, 1, 1);
        gPane.add(startDate, 1, 2);
        gPane.add(endDate, 1, 3);
        gPane.add(cBox, 1, 4);
        gPane.add(noButton, 1, 5);
    }
    
    private void initListeners(){
        //set up listeners now
        EventHandler yesButtonHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            yesOrNo = true;
            this.close();
        };
        EventHandler noButtonHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            yesOrNo = false;
            //make this stuff go away
            this.close();
        };
        yesButton.setOnAction(yesButtonHandler);
        noButton.setOnAction(noButtonHandler);
        
        //this is xml information extraction
        manager = PropertiesManager.getPropertiesManager();
        
        //scenes and stages and stuff
        Scene s = new Scene(gPane);
        URL resource = this.getClass().getClassLoader().getResource(
                manager.getProperty(PropertyType.DIRECTORY));
        String resourceString = resource.toExternalForm();
        s.getStylesheets().add(resourceString);
        gPane.getStyleClass().add(manager.getProperty(
                PropertyType.ADD_EDIT_DIALOG));
        this.setScene(s);
    }
    
    public void showWithTitle(String title){
        this.setTitle(title);
        this.showAndWait();
    }
    
    public ToDoItem getItem(){
        if(yesOrNo){
        return new ToDoItem(categoryField.getText(),
            descriptionField.getText(),
            startDate.getValue(),
            endDate.getValue(),
            cBox.isSelected());
        }
        else{
            return null;
        }
    }
}
