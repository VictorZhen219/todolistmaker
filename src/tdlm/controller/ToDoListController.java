package tdlm.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import properties_manager.PropertiesManager;
import tdlm.ToDoItemAddOrEditDialog;
import tdlm.gui.Workspace;
import tdlm.data.DataManager;
import saf.AppTemplate;
import tdlm.data.ToDoItem;
import saf.ui.AppYesNoCancelDialogSingleton;
import tdlm.PropertyType;

/**
 * This class responds to interactions with todo list editing controls.
 * 
 * @author McKillaGorilla
 * @version 1.0
 */
public class ToDoListController {
    AppTemplate app;
    
    public ToDoListController(AppTemplate initApp) {
	app = initApp;
    }
    
    public void processAddItem() {
        //is this where my stuff goes????
        //yes it is
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ToDoItemAddOrEditDialog dialog = new ToDoItemAddOrEditDialog();
        dialog.showWithTitle(props.getProperty(PropertyType.ADD_TO_DO_ITEM));
        ToDoItem toAdd = dialog.getItem();
        if(toAdd==null){
            //??
        }       
        else{
            //add to the databases
            DataManager manager = (DataManager) app.getDataComponent();
            manager.addItem(toAdd);
        }
	// ENABLE/DISABLE THE PROPER BUTTONS
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
    }
    
    public void processRemoveItem(ToDoItem toRemove) {
        AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        dialog.show(props.getProperty(PropertyType.REMOVAL),
                props.getProperty(PropertyType.CONFIRM_REMOVAL));
        if(!dialog.getSelection().equals("Yes")){
            dialog.close();
        }
        else{
            DataManager manager = (DataManager) app.getDataComponent();
            //the item to remove is the one that is selected.
            manager.removeItem(toRemove);

            Workspace workspace = (Workspace)app.getWorkspaceComponent();
            workspace.reloadWorkspace();
      
        }
    }
    
    public void processMoveUpItem(ToDoItem item) {
        //swap places in the array list
        DataManager manager = (DataManager) app.getDataComponent();
        int positionToAdd = manager.getItems().indexOf(item);
        //we have positiontoadd and positiontoadd-1
        manager.getItems().remove(positionToAdd);
        manager.getItems().add(positionToAdd-1, item);
        
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
    }
    
    public void processMoveDownItem(ToDoItem item) {
        //swap places in the array list
        DataManager manager = (DataManager) app.getDataComponent();
        int positionToAdd = manager.getItems().indexOf(item);
        //we have positiontoadd and positiontoadd+1
        manager.getItems().remove(positionToAdd);
        manager.getItems().add(positionToAdd+1, item);
        
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
    }
    
    public void processEditItem(ToDoItem toEdit) {
        ToDoItemAddOrEditDialog dialog = new ToDoItemAddOrEditDialog(toEdit);
        dialog.showWithTitle("Edit To Do Item");
        ToDoItem newProperties = dialog.getItem();
        if(newProperties!=null){
            toEdit.setCategory(newProperties.getCategory());
            toEdit.setCompleted(newProperties.getCompleted());
            toEdit.setDescription(newProperties.getDescription());
            toEdit.setStartDate(newProperties.getStartDate());
            toEdit.setEndDate(newProperties.getEndDate());
        }
        
        
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
    }
}
