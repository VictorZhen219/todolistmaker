package tdlm.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import tdlm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES
    
    // NAME OF THE TODO LIST
    String name;
    
    // LIST OWNER
    String owner;
    
    // THESE ARE THE ITEMS IN THE TODO LIST
    ArrayList<ToDoItem> items;
    
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        items = new ArrayList<ToDoItem>();
        name = "";
        owner = "";
    }
    
    public ArrayList<ToDoItem> getItems() {
	return items;
    }
    
    public String getName() {
        return name;
    }
    
    public String getOwner() {
        return owner;
    }

    public void addItem(ToDoItem item) {
        items.add(item);
    }
    
    public void removeItem(ToDoItem item) {
        items.remove(item);
    }
    
    public void setName(String initName){
        name = initName;
    }
    
    public void setOwner(String initOwner){
        owner = initOwner;
    }

    /**
     * TODO: clear stuff
     */
    @Override
    public void reset() {
        //should clear the textfields too
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.setNameAndOwnerFieldsToNothing();
        items.clear();
    }
    
    public void updateNameAndOwnerTextFields(String name, String owner) {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.updateNameAndOwnerTextFields(name, owner);
    }
}